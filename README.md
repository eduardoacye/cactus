# cactus

Simple experiment consisting of a set of programs that manipulate interpreted
language code.

The idea behind `cactus` is that simple comands embeded in the comment lines of
a source file can be useful for instructing how the file is interpreted and
displayed. Instead of having special syntax and file formats to process source
code, `cactus` has the philosophy that the source code has to be runnable by
itself and other programs read the source code paying attention to specific
comment lines to determine how to work with the file.

The experiment consist of

- A configuration notation of how the file(s) are processed
- Output source files to HTML and LaTeX
- Literate programming runnable programs
- Notebook style runnable programs

The experiment is done in a laboratory where the Scheme programming language
**rifa**.

The implementation I use for running my code is **Larceny Scheme**

Contact me fo'mo'info' at `eduardo.acye@gmail.com`.
